# METAMORPH

## About the project

The METAMORPH project (the name is an abbreviation meaning "Mobile Engineering Testing Architecture - MediaTek Open Research Platform for Hackers") is an effort that pursues two goals:

1. Collect as much information as possible in order to document MediaTek's META mode in their feature phone (MT62xx) and smartphone (at least MT65xx/MT67xx) chipsets, and create detailed open documentation about them;
2. Using this information, create open and cross-platform software tools to interact with actual phones, that can serve as a viable alternative to closed vendor tools like MauiMETA.

The project is not meant to be a part of BananaHackers initiative but some of its results may also be useful when dealing with MTK-based KaiOS phones.

Most essential things are planned to be documented in this README file but some detailed descriptions, schematics or tables may require putting them into separate files in this repo. Any future codebase will be created in the `src` directory.

## About META mode

META (stands for Mobile Engineering Testing Architecture) is MediaTek's proprietary subsystem for direct interaction with devices' radio modules. It allows performing low-level system tests, direct NVRAM access (including but not limited to IMEI manipulation and band tweaking), radio calibration, network monitoring, audio parameter tweaking, raw GSM traffic logging and more interesting things.

## Entering META mode

This is the general method to enter the META mode when the device is turned off. Some phones also allow rebooting into META with AT commands or booting with special key combos but we're going to cover the most universal method that should work everywhere and that's actually used by the MauiMETA tool itself.

So, to enter META mode, we catch the moment the preloader port is opened when connecting the turned-off phone to the PC. Then we send `METAMETA` string into the preloader port. The port should respond with `READYATE` (the response may vary depending on the chipset) and the phone will boot into the META mode.

Some devices also allow advanced META debugging and logging even after the normal boot. To enter this "hybrid" mode, send `ADVEMETA` to the preloader port.

## Checking if the phone is in the META mode

Once in META mode, the device opens a new serial port over USB. Normal scanning for "bulk" transfer interfaces will not just work (there might be several of them), we need to have the following parameters matching:

- `interfaceClass`: 10
- `interfaceSubclass`: 0
- `interfaceProtocol`: 0

(the necessary filter object for [USBIO](https://gist.github.com/plugnburn/e011984f0feb522d23ddadbd73455fb1) would be: `{classCode: 10, subclassCode: 0, protocolCode: 0}`)

For other cases, let OS detect the port as standard USB-Serial and create some new `tty` device in `/dev` filesystem.

## Some useful properties of the META mode

- On the devices with smartphone chipsets (MT65xx/MT67xx/MT69xx), ADB access is also turned on after entering META mode (it's still not rooted though).

## META message structure

Each META message (command) consists of two parts: request and confirmation. Depending on the Message ID (that determines the type of the message), the request and confirmation can have different data structures.



## Tested META messages

In this section, only known and tested META messages will be listed.

## Known META commands (untested yet)

In this section, commands found all over the Internet will be put with brief description. Once they are tested, they will be moved into the "Tested META messages" section.

### IMEI manipulation


## Known META message IDs

name|value (dec)|value (hex)|description
----|-----------|-----------|-----------

## Known META result codes

The META message can be responded with two codes: `META_STATUS` (0 if failed, 1 if succeeded) and `META_RESULT`. This is the table of possible results (on MT6572):

name|value (dec)|value (hex)|description
----|-----------|-----------|-----------
META_SUCCESS|0|0|Operation successful
META_FAILED|1|1|Operation failed with unknown reason (?)
META_COMM_FAIL|2|2|
META_NORESPONSE|3|3|
META_BUFFER_LEN|4|4|
META_FILE_BAD|5|5|
META_LID_INVALID|6|6|
META_INTERNAL_DB_ERR|7|7|
META_NO_MEMORY|8|8|
META_INVALID_ARGUMENTS|9|9|
META_TIMEOUT|10|a|
META_BUSY|11|b|
META_INVALID_HANDLE|12|c|
META_FAT_ERROR|13|d|
META_FAT_DISK_FULL|14|e|
META_FAT_ROOT_DIR_FULL|15|f|
META_FAT_INVALID_FILENAME|16|10|
META_FAT_INVALID_FILE_HANDLE|17|11|
META_FAT_FILE_NOT_FOUND|18|12|
META_FAT_DRIVE_NOT_FOUND|19|13|
META_FAT_PATH_NOT_FOUND|20|14|
META_FAT_ACCESS_DENIED|21|15|
META_FAT_TOO_MANY_FILES|22|16|
META_INCORRECT_TARGET_VER|23|17|
META_COM_ERROR|24|18|
META_BROM_CMD_ERROR|25|19|
META_INCORRECT_BBCHIP_TYPE|26|1a|
META_BROM_ERROR|27||
META_STOP_BOOTUP_PROCEDURE|28||
META_CANCEL|29||
META_CCT_NOT_IMPORT_PROFILE|30||
META_CCT_INVALID_SENSOR_ID|31||
META_CCT_TGT_NO_MEM_FOR_SINGLE_SHOT|32||
META_CCT_TGT_NO_MEM_FOR_MULTI_SHOT|33||
META_FUNC_NOT_IMPLEMENT_YET|34||
META_CCT_NOT_IMPLEMENT_YET|34||Same as META_FUNC_NOT_IMPLEMENT_YET
META_CCT_PREVIEW_ALREADY_STARTED|35||
META_CCT_PREVIEW_ALREADY_STOPPED|36||
META_CCT_READ_REG_NO_CNF|||
META_CCT_WRITE_REG_NO_CNF|||
META_CCT_TGT_ABORT_IMAGE_SENDING|||
META_CCT_READ_ONLY_REGISTER|||
META_CCT_LOAD_FROM_NVRAM_FAIL|||
META_CCT_SAVE_TO_NVRAM_FAIL|||
META_CCT_AE_INVALID_EC_LEVEL|||
META_CCT_AE_INVALID_EC_STEP|||
META_CCT_AE_ALREADY_ENABLED|||
META_CCT_AE_ALREADY_DISABLED|||
META_CCT_WB_INVALID_INDEX|||
META_CCT_NO_TGT_SENSOR_MATCH_IN_PROFILE|||
META_CCT_IMAGE_SENDING_FAME_NUM_ERROR|||
META_CCT_AE_IS_NOT_DISABLED|||
META_FAT_APP_QUOTA_FULL|||
META_IMEI_CD_ERROR|||
META_RFID_MISMATCH|||
META_NVRAM_DB_IS_NOT_LOADED_YET|||
META_CCT_ERR_CAPTURE_WIDTH_HEIGHT_TOO_SMALL|||
META_WAIT_FOR_TARGET_READY_TIMEOUT|||
META_CCT_ERR_SENSOR_ENG_SET_INVALID_VALUE|||
META_CCT_ERR_SENSOR_ENG_GROUP_NOT_EXIST|||
META_CCT_NO_TGT_ISP_MATCH_IN_PROFILE|||
META_CCT_TGT_ISP_SUPPORT_NOT_DEFINED|||
META_CCT_ERR_SENSOR_ENG_ITEM_NOT_EXIST|||
META_CCT_ERR_INVALID_COMPENSATION_MODE|||
META_CCT_ERR_USB_COM_NOT_READY|||
META_CCT_DEFECTPIXEL_CAL_UNDER_PROCESSING|||
META_CCT_ERR_DEFECTPIXEL_CAL_NO_MEM|||
META_CCT_ERR_TOO_MANY_DEFECT_PIXEL|||
META_CCT_ERR_CAPTURE_JPEG_FAIL|||
META_CCT_ERR_CAPTURE_JPEG_TIMEOUT|||
META_CCT_ERR_AF_FAIL|||
META_CCT_ERR_AF_TIMEOUT|||
META_CCT_ERR_AF_LENS_OFFSET_CAL_FAIL|||
META_CCT_ERR_PREVIEW_MUST_ENABLE|||
META_CCT_ERR_UNSUPPORT_CAPTURE_FORMAT|||
META_CCT_ERR_EXCEED_MAX_DEFECT_PIXEL|||
META_ERR_EXCEED_MAX_PEER_BUF_SIZE|||
META_CCT_ERR_INVALID_WIDTH_FACTOR|||
META_BROM_SECURITY_CHECK_FAIL|||
META_CCT_ERR_PREVIEW_MUST_DISABLE|||
META_MAUI_DB_INCONSISTENT|||
META_FAT_FILEPATH_TOO_LONG|||
META_FAT_RESTRICTED_FILEPATH|||
META_FAT_DIR_NOT_EXIST|||
META_FAT_DISK_SPACE_IS_NOT_ENOUGH|||
META_TDMB_ERR_BAND_NOT_EXIST|||
META_TDMB_ERR_FREQ_NOT_EXIST|||
META_TDMB_ERR_ENSM_NOT_EXIST|||
META_TDMB_ERR_SERV_NOT_EXIST|||
META_TDMB_ERR_SUB_CHAN_NOT_EXIST|||
META_TDMB_ERR_DEMOD_STATE|||
META_PERMISSION_DENIED|||
META_ENUMERATE_USB_FAIL|||
META_STOP_ENUM_USB_PROCEDURE|||
META_CCT_6238_AE_ALREADY_ENABLED|||
META_CCT_6238_AE_ALREADY_DISABLED|||
META_CCT_6238_AE_IS_NOT_DISABLED|||
META_CCT_6238_ISP_FLASHLIGHT_LINEARITY_PRESTROBE_FAIL|||
META_NOT_SUPPORT|||
META_LAST_RESULT|||

Not all of these result codes may be used in the actual device.

## Reference hardware available for testing

- `MT6261D`
- `MT6261M`
- `MT6260A`
- `MT6276`
- `MT6572` (primary testing)
- `MT6737M`

